﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class AutoGenerateCourse : System.Web.UI.Page
{
    string[] course_pass_code;
    double[] course_grade;
    string[] current_course;
    string[] total_course;
    string[] pre_req;
    string course = null;
    double grade;
    static double CGPA;
    int warning;
    static int semester_student;
    string course_fast;
    string credit;
    string code;
    string precode;
    static int prereqlength = 0;
    static List<string> list1 = new List<string>();
    static List<string> total_courselist = new List<string>();
    static List<string> total_fastcourses = new List<string>();
    static List<string> statuslist = new List<string>();
    static List<string> studentcoursecodelist = new List<string>();
    static List<string> currentcourse = new List<string>();
    static List<string> currentcredits = new List<string>();
    static List<string> prereqfastcourses = new List<string>();
    string[] course_combination = new string[50];
    static List<string> setcoursestudentlist = new List<string>();
    static List<double> studentcoursegrade = new List<double>();
    static List<string> studenttakingcourses = new List<string>();
    static string mysemestercount;



    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserName"] == "null")
        {
            //MessageBox
            Response.Redirect("Login.aspx");
        }
        else
        {

            Label1.Text = (string)Session["UserName"];

        }


        if (!IsPostBack)
        {


            Connection1.connect();
            DataTable gt = Connection1.getcourses();
            Connection1.disconnect();

            if (gt.Rows.Count == 0)
            {
                Response.Write("<script LANGUAGE='JavaScript' >alert('No Record Found')</script>");
            }
            for (int i = 0; i < gt.Rows.Count; i++)
            {

                object a = gt.Rows[i]["COURSENAME"];
                object b = gt.Rows[i]["CREDITHOURS"];
                object c = gt.Rows[i]["COURSECODE"];
                object d = gt.Rows[i]["PREREQ"];

                course_fast = course_fast + "," + a.ToString();
                credit = credit + "," + b.ToString();
                code = code + "," + c.ToString();
                precode = precode + "," + d.ToString();



            }
            string[] every_course = course_fast.Split(new Char[] { ',', '\n' });
            string[] every_credit_hours = credit.Split(new Char[] { ',', '\n' });
            string[] every_course_code = code.Split(new Char[] { ',', '\n' });
            string[] every_course_prereq = precode.Split(new Char[] { ',', '\n' });
            prereqlength = every_course.Length;
            for (int j = 0; j < every_course_prereq.Length; j++)
            {
                list1.Add(every_course_prereq[j]);
                total_courselist.Add(every_course_code[j]);
                total_fastcourses.Add(every_course[j]);
            }


            Connection1.connect();
            DataTable gt1 = Connection1.getStudentcourses();
            Connection1.disconnect();
            object t1 = gt1.Rows[0]["STATUSCOURSE"];
            object b1 = gt1.Rows[0]["COURSECODE"];
            object b2 = gt1.Rows[0]["CGPA"];
            object b3 = gt1.Rows[0]["COURSEGRADE"];
            object b4 = gt1.Rows[0]["WARNINGCOUNT"];
            object b5 = gt1.Rows[0]["SEMESTERCOUNT"];
            object b6 = gt1.Rows[0]["COURSES"];


            string[] every_course_code_student = b1.ToString().Split(new Char[] { ',', '\n' });
            string[] every_course_status_student = t1.ToString().Split(new Char[] { ',', '\n' });
            string[] every_course_grade_student = b3.ToString().Split(new Char[] { ',', '\n' });
            string[] every_course_name_student = b6.ToString().Split(new Char[] { ',', '\n' });
            CGPA = Convert.ToDouble(b2.ToString());
            warning = Convert.ToInt32(b4.ToString());
            semester_student = Convert.ToInt32(b5.ToString());
            for (int k = 0; k < every_course_code_student.Length; k++)
            {
                studentcoursecodelist.Add(every_course_code_student[k]);
                statuslist.Add(every_course_status_student[k]);
                studenttakingcourses.Add(every_course_name_student[k]);
                studentcoursegrade.Add(Convert.ToDouble(every_course_grade_student[k]));

            }
            Connection1.connect();
            DataTable gt2 = Connection1.getCurrentCourses();
            Connection1.disconnect();
            object z1 = gt2.Rows[0]["COURSENAME"];
            object z2 = gt2.Rows[0]["CREDITHOURS"];

            string[] every_current_course_offered = z1.ToString().Split(new Char[] { ',', '\n' });
            string[] every_course_credit_offered = z2.ToString().Split(new Char[] { ',', '\n' });

            for (int i = 0; i < every_course_credit_offered.Length; i++)
            {
                //CheckBoxList1.Items.Add(every_current_course_offered[i]);
                currentcourse.Add(every_current_course_offered[i]);
                currentcredits.Add(every_course_credit_offered[i]);

            }

            course_pass_code = new string[] { "CS101", "MT101", "MT104", "NS101", "SS102" };
            course_grade = new double[] { 1.0, 1.0, 2.0, 1.0, 1.0 };
            current_course = new string[] { "CS101", "MT101", "MT104", "NS101", "SS102", "CS103", "EE109", "MT115", "SS122", "SS138", "CS201", "CS211", "EE213", "SS111", "MGX01" };
            total_course = new string[] { "CS101", "MT101", "MT104", "NS101", "SS102", "CS103", "EE109", "MT115", "SS122", "SS138", "CS201", "CS211", "EE213", "SS111", "MGX01" };
            pre_req = new string[] { "0", "0", "0", "0", "0", "CS101", "0", "MT101", "SS102", "0", "CS103", "0", "EE109", "0", "0" };




        }
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
           ListBox1.Items.Clear();
        ListBox2.Items.Clear();
         List<string> semsterlistfor_prereq = new List<string>();
         List<string> semsterlistfor_coursename = new List<string>();
         List<string> semsterlistfor_coursecode = new List<string>();
         String semestercoursename="";
         String semestercoursecode="";
         String semestercoursprereq="";
         bool mytest = false;

         int setvalue=1;
        string semester;
        if (CGPA>2)
        {
             
            if (semester_student < 8)
                semester_student = semester_student + 1;
            semester = semester_student.ToString();
            Connection1.connect();
            DataTable gt = Connection1.getsemestercourse(semester);
            Connection1.disconnect();
            for (int i = 0; i < gt.Rows.Count; i++)
            {

                object a = gt.Rows[i]["COURSENAME"];
                object b = gt.Rows[i]["CREDITHOURS"];
                object c = gt.Rows[i]["COURSECODE"];
                object d = gt.Rows[i]["PREREQ"];

                 semestercoursename = semestercoursename + "," + a.ToString();
                 semestercoursprereq = semestercoursprereq + "," + d.ToString();
                 semestercoursecode = semestercoursecode + "," + c.ToString();

            }
           
            string[] every_course_semester = semestercoursename.Split(new Char[] { ',', '\n' });
            string[] every_course_prereq_semester = semestercoursprereq.Split(new Char[] { ',', '\n' });
            string[] every_course_code_semester = semestercoursecode.Split(new Char[] { ',', '\n' });
            for (int i = 0; i < every_course_semester.Length; i++)
            {
                semsterlistfor_coursename.Add(every_course_semester[i]);
                semsterlistfor_coursecode.Add(every_course_code_semester[i]);
                semsterlistfor_prereq.Add(every_course_prereq_semester[i]);
            }

            for (int i = 1; i < every_course_semester.Length; i++)
            {
                ListBox2.Items.Add(semsterlistfor_coursename[i]);
            }
            for (int i = 1; i < currentcourse.Count; i++)
            {
                ListBox3.Items.Add(currentcourse[i]);
            }



            for (int i = 0; i < studentcoursegrade.Count; i++)
            {
                if (studentcoursegrade[i] == 0)
                {
                    
                    for (int j = 0; j < semsterlistfor_prereq.Count; j++)
                    {
                        if (studentcoursecodelist[i] == semsterlistfor_prereq[j])
                        {
                            
                            semsterlistfor_coursename.Remove(semsterlistfor_coursename[j]);
                        }
                    }
                }
            }

           
            for(int x=1;x<semsterlistfor_prereq.Count; x++)
            {
               
                mytest = false;
                for (int j = 0; j < studentcoursecodelist.Count; j++)
                {
                   
                    if (semsterlistfor_prereq[x]!="0")
                    {
                        
                        if (semsterlistfor_prereq[x] == studentcoursecodelist[j])
                        {
                            
                            mytest = true;
                        }
                    }
                    if (semsterlistfor_prereq[x] == "0")
                    {
                       
                        mytest = true;
                        j = studentcoursecodelist.Count;
                        
                    }
                }
                if (mytest == false)
                {
                    
                   // ListBox1.Items.Add(semsterlistfor_coursename[setvalue]);
                    //ListBox1.Items.Add(semsterlistfor_prereq[x]);
                    //semsterlistfor_coursename.Remove(semsterlistfor_coursename[x]);
                    semsterlistfor_coursename[x] = "a";
                    
                    
                    
                }
                

            }
          
            for (int i = 1; i < semsterlistfor_coursename.Count; i++)
            {
                if (semsterlistfor_coursename[i] == "a")
                    semsterlistfor_coursename.Remove(semsterlistfor_coursename[i]);
            }
            for (int i = 1; i < semsterlistfor_coursename.Count; i++)
            {
                if (semsterlistfor_coursename[i] == "a")
                    semsterlistfor_coursename.Remove(semsterlistfor_coursename[i]);
            }
            for (int i = 1; i < semsterlistfor_coursename.Count; i++)
            {
                if (semsterlistfor_coursename[i] == "a")
                    semsterlistfor_coursename.Remove(semsterlistfor_coursename[i]);
            }
            for (int i = 1; i <semsterlistfor_coursename.Count; i++)
            {
                ListBox1.Items.Add(semsterlistfor_coursename[i]);
            }
           
        }
        semsterlistfor_coursename.Clear();
        semsterlistfor_coursecode.Clear();
        semsterlistfor_prereq.Clear();
        semester_student--;
    }
    
}