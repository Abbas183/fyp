﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SelectCourses.aspx.cs" Inherits="SelectCourses" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">


<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
  <meta charset="utf-8">

  
<!--initiate accordion-->
<script type="text/javascript">

    $(function () {

        var menu_ul = $('.menu > li > ul'),

       menu_a = $('.menu > li > a');

        menu_ul.hide();


        menu_a.click(function (e) {
            e.preventDefault();

            if (!$(this).hasClass('active')) {

                menu_a.removeClass('active');
                menu_ul.filter(':visible').slideUp('normal');

                $(this).addClass('active').next().stop(true, true).slideDown('normal');
            } else {

                $(this).removeClass('active');
                $(this).next().stop(true, true).slideUp('normal');

            }
        });

    });
</script>

  <link rel="stylesheet" href="css/styles.css">
  <link rel="stylesheet" href="Styles/Form.css">
  <link rel="stylesheet" media="screen" href="styles.css" >

		
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>

<link href="templatemo_style.css" rel="stylesheet" type="text/css" />
		
		<script src="js/jquery-1.8.3.min.js"></script>
		<script src="css/5grid/init.js?use=mobile,desktop,1000px&amp;mobileUI=1&amp;mobileUI.theme=none&amp;mobileUI.titleBarOverlaid=1"></script>
</head>
<body style="background-color:#C1C0C0">
    
    <form id="form2" runat="server">
    
   
    
    <div style="background-color:Gray; opacity:120" >
  

		<!-- Nav -->
			<nav id="nav" class="mobileUI-site-nav">
				<ul>
					<li class=""><a href="">Advisory System Fast-NU</a></li>
					
				</ul>
			</nav>
		<!-- /Nav -->
       
   <div style="color:White; float:left; width:203px; margin-top:55px">
 

 	<ul class="menu" style="text-decoration:none">
	
		
	<li class="item0" style=" background-repeat:repeat-x; color:Blue"><a href="#" >Menu </a>	</li>
   
		
<li class="item1" ><a href="Grades.aspx" >Grades Combination </a></li>
		<li class="item2"><a href="SelectCourses.aspx">Registration </a> </li>
		<li class="item3"><a href="AutoGenerateCourse.aspx"> View Course </a>	</li>
		<li class="item4"><a href="#">Calculate GPA </a>	</li>
		<li class="item5"><a href="SubmitApp.aspx">Submit Application </a>	</li>
	</ul>


</div>
 <div class='widget ContactForm' id='ContactForm1' style="margin-top:55px; margin-left:60px; float:right; margin-right:110px">
  <div class='contact-form-widget' style="width:900px; height:800px ">
    <div class='form'>
      
    
    <div class="contact_form" >
   
 <ul>
 <li style="margin-top:0px">

 <div style=" text-align:center">
             <h2 > Registration</h2>
            
            </div>

      <br />
   </li>
    
        <br />
    <li style="height:300px;"><div style="margin-left:200px; width: 442px;"> 
    <asp:CheckBoxList ID="CheckBoxList1" runat="server" Width="609px"></asp:CheckBoxList>
     </div>
     </li>
     <li>
     <div>
    <asp:Button ID="Button1" runat="server" Text="Submit"  BackColor="ActiveCaption" 
            BorderColor="Black" BorderWidth="1" onclick="Button1_Click" Width="180px"></asp:Button>
            </div>
    <br />
    </li>
    <li style="height:100px;">
    <div style="margin-left:200px">
    <asp:ListBox ID="ListBox1" runat="server" Width="422px"></asp:ListBox>
    </div>
    </li>
    </ul>
    </div>

    </div>
  </div>
</div>
   </div>
   


    </form>
</body>
</html>
