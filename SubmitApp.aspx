﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SubmitApp.aspx.cs" Inherits="SubmitApp" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
  <meta charset="utf-8">

  
<!--initiate accordion-->
<script type="text/javascript">

    $(function () {

        var menu_ul = $('.menu > li > ul'),

       menu_a = $('.menu > li > a');

        menu_ul.hide();


        menu_a.click(function (e) {
            e.preventDefault();

            if (!$(this).hasClass('active')) {

                menu_a.removeClass('active');
                menu_ul.filter(':visible').slideUp('normal');

                $(this).addClass('active').next().stop(true, true).slideDown('normal');
            } else {

                $(this).removeClass('active');
                $(this).next().stop(true, true).slideUp('normal');

            }
        });

    });
</script>

  <link rel="stylesheet" href="css/styles.css">
  <link rel="stylesheet" href="Styles/Form.css">
  <link rel="stylesheet" media="screen" href="styles.css" >

		
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>

<link href="templatemo_style.css" rel="stylesheet" type="text/css" />
		
		<script src="js/jquery-1.8.3.min.js"></script>
		<script src="css/5grid/init.js?use=mobile,desktop,1000px&amp;mobileUI=1&amp;mobileUI.theme=none&amp;mobileUI.titleBarOverlaid=1"></script>
</head>
<body style="background-color:#C1C0C0">
    
    <form id="form2" runat="server">
    
   
    
    <div style="background-color:Gray; opacity:120" >
  

		<!-- Nav -->
			<nav id="nav" class="mobileUI-site-nav">
				<ul>
					<li class=""><a href="">Advisory System Fast-NU</a></li>
					
				</ul>
			</nav>
		<!-- /Nav -->
       
   <div style="color:White; float:left; width:203px; margin-top:55px">
 

 	<ul class="menu" style="text-decoration:none">
	
		
	<li class="item0" style=" background-repeat:repeat-x; color:Blue"><a href="#" >Menu </a>	</li>
   
		
	<li class="item1" ><a href="Grades.aspx" >Grades Combination </a></li>
		<li class="item2"><a href="SelectCourses.aspx">Registration </a> </li>
		<li class="item3"><a href="AutoGenerateCourse.aspx"> View Course </a>	</li>
		<li class="item4"><a href="#">Calculate GPA </a>	</li>
		<li class="item5"><a href="SubmitApp.aspx">Submit Application </a>	</li>
	</ul>


</div>
 <div class='widget ContactForm' id='ContactForm1' style="margin-top:55px; margin-left:60px; float:right; margin-right:110px">
  <div class='contact-form-widget' style="width:900px; height:800px ">
    <div class='form'>
      
      <div class="contact_form" >
   
 <ul>
 <asp:Label ID="Lablemessage" runat="server" Text="" ForeColor="Red"></asp:Label>
        <li style="margin-top:0px">

             <h2>Applicatipn</h2>
            
 <span class="required_notification">* Fill Required Field</span>
             <br />
     
   </li>
   <br />

   <li>
     <label for="name">Roll No:</label>
         

   <asp:TextBox ID="TextBox1" runat="server" Width="200px" required></asp:TextBox>
   <br />

     <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ForeColor="Red" runat="server" ErrorMessage="Please Enter Roll number" Display="Dynamic"
    ControlToValidate="TextBox1"></asp:RequiredFieldValidator>
     
   </li>
   <br />
     <li>
   
     <label for="name">CGPA:</label>
         

   <asp:TextBox ID="TextBox8" runat="server" Width="200px" required></asp:TextBox>
   <br />
   <asp:RangeValidator ID="RangeValidator1" runat="server"  ControlToValidate="TextBox8" Type="Double" minimumvalue="0.00" maximumvalue="4.0000" ErrorMessage=" CGPA is not Correct" ForeColor="Red"></asp:RangeValidator>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ForeColor="Red" runat="server" ErrorMessage="Please Enter CGPA" Display="Dynamic"
    ControlToValidate="TextBox8"></asp:RequiredFieldValidator>

     
 
   </li>
     <br />
        <li>
            <label for="name">Name:</label>
         

   <asp:TextBox ID="TextBox2" runat="server" Width="200px" required></asp:TextBox>
   <br />
    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ForeColor="Red" runat="server" ErrorMessage="Please Enter Roll Number" Display="Dynamic"
    ControlToValidate="TextBox2"></asp:RequiredFieldValidator>
     
   </li>
       
        <li  style="height:300px">
            <label for="email">Subjects </label>
            <br />
            <br />
            
    
      <span style="margin-left:90px">
   <asp:TextBox ID="TextBox3" runat="server" Width="200px" required 
                ontextchanged="TextBox3_TextChanged"></asp:TextBox><span style="margin-left:200px">   <asp:TextBox ID="TextBox4" runat="server" Width="200px" required></asp:TextBox></span>
   </span>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ForeColor="Red" runat="server" ErrorMessage="Please Enter Subject" Display="Dynamic"
    ControlToValidate="TextBox3"></asp:RequiredFieldValidator>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" ForeColor="Red" runat="server" ErrorMessage="Please Enter Subject" Display="Dynamic"
    ControlToValidate="TextBox4"></asp:RequiredFieldValidator>
   <br />
      
         <br />
   <span style="margin-left:90px">
   <asp:TextBox ID="TextBox5" runat="server" Width="200px" required></asp:TextBox><span style="margin-left:200px">   <asp:TextBox ID="TextBox6" runat="server" Width="200px" required></asp:TextBox></span>
   </span>
   <asp:RequiredFieldValidator ID="RequiredFieldValidator6" ForeColor="Red" runat="server" ErrorMessage="Please Enter Subject" Display="Dynamic"
    ControlToValidate="TextBox5"></asp:RequiredFieldValidator>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator7" ForeColor="Red" runat="server" ErrorMessage="Please Enter Subject" Display="Dynamic"
    ControlToValidate="TextBox6"></asp:RequiredFieldValidator>
    <br />
      
         <br />
   <span style="margin-left:90px">
   <asp:TextBox ID="TextBox7" runat="server" Width="200px" required></asp:TextBox><span style="margin-left:200px"> </span>
   </span>
   <asp:RequiredFieldValidator ID="RequiredFieldValidator8" ForeColor="Red" runat="server" ErrorMessage="Please Enter Subject" Display="Dynamic"
    ControlToValidate="TextBox7"></asp:RequiredFieldValidator>
        </li>
      

       
        <li style="height:100px">
        <br />

            <asp:Button ID="Button1" runat="server" Text="  Submit"  CssClass="submit" 
                Width="150px" Font-Bold="true"  ForeColor="White" BackColor="BlueViolet" onclick="Button1_Click"  
                />

    

    <br />
    <br />
    </li>
    </ul>
</form>


    </div>
  </div>
</div>
   </div>
   


    </form>
</body>
</html>