﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.OleDb;
using System.Data.Odbc;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Login : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["UserName"] = "null";
    }
    protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
    {


        string name = TextBox1.Text;
        string pass = TextBox2.Text;

        Connection.connect();
        DataTable log = Connection.authorised(name, pass);
        
     //   Connection.disconnect();  //sql connection disconnect

        if (log.Rows.Count > 0)
        {
            Session["UserName"] =name ;
            Response.Redirect("StudentProfile.aspx");
        }
        else
        {
          //  MessageBox.Show("You have Entered wrong Name or Password. Please Retry");
            TextBox1.Text = "";
            TextBox2.Text = "";

            Label1.Text = "Wrong Attempt";

        }

    }
}