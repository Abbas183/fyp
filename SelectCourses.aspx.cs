﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

using System.Data.OleDb;

public partial class SelectCourses : System.Web.UI.Page
{

    static string student_name;
    static string student_rollnumber;

    string course = null;
    double grade;
    static double CGPA;
    int warning;
    static int semester_student;
    string course_fast;
    string credit;
    string code;
    string precode;
    static int prereqlength = 0;
    static List<string> list1 = new List<string>();
    static List<string> total_courselist = new List<string>();
    static List<string> total_fastcourses = new List<string>();
    static List<string> statuslist = new List<string>();
    static List<string> studentcoursecodelist = new List<string>();
    static List<string> currentcourse = new List<string>();
    static List<string> currentcredits = new List<string>();
    static List<string> prereqfastcourses = new List<string>();
    string[] course_combination = new string[50];
    static List<string> setcoursestudentlist = new List<string>();
    static List<double> studentcoursegrade = new List<double>();
    static List<string> studenttakingcourses = new List<string>();
    static List<string> submit_application_courses = new List<string>();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserName"] == "null")
        {
            //MessageBox
            Response.Redirect("Login.aspx");
        }
        else
        {

            Label1.Text = (string)Session["UserName"];

        }


        if (!IsPostBack)
        {
            Connection1.connect();
            DataTable gt = Connection1.getcourses();
            Connection1.disconnect();

            if (gt.Rows.Count == 0)
            {
                Response.Write("<script LANGUAGE='JavaScript' >alert('No Record Found')</script>");
            }
            for (int i = 0; i < gt.Rows.Count; i++)
            {

                object a = gt.Rows[i]["COURSENAME"];
                object b = gt.Rows[i]["CREDITHOURS"];
                object c = gt.Rows[i]["COURSECODE"];
                object d = gt.Rows[i]["PREREQ"];

                course_fast = course_fast + "," + a.ToString();
                credit = credit + "," + b.ToString();
                code = code + "," + c.ToString();
                precode = precode + "," + d.ToString();



            }
            string[] every_course = course_fast.Split(new Char[] { ',', '\n' });
            string[] every_credit_hours = credit.Split(new Char[] { ',', '\n' });
            string[] every_course_code = code.Split(new Char[] { ',', '\n' });
            string[] every_course_prereq = precode.Split(new Char[] { ',', '\n' });
            prereqlength = every_course.Length;
            for (int j = 0; j < every_course_prereq.Length; j++)
            {
                list1.Add(every_course_prereq[j]);
                total_courselist.Add(every_course_code[j]);
                total_fastcourses.Add(every_course[j]);
            }


            Connection1.connect();
            DataTable gt1 = Connection1.getStudentcourses();
            Connection1.disconnect();
            object t1 = gt1.Rows[0]["STATUSCOURSE"];
            object b1 = gt1.Rows[0]["COURSECODE"];
            object b2 = gt1.Rows[0]["CGPA"];
            object b3 = gt1.Rows[0]["COURSEGRADE"];
            object b4 = gt1.Rows[0]["WARNINGCOUNT"];
            object b5 = gt1.Rows[0]["SEMESTERCOUNT"];
            object b6 = gt1.Rows[0]["COURSES"];
            object b7 = gt1.Rows[0]["ID"];
            object b8 = gt1.Rows[0]["NAME"];


            string[] every_course_code_student = b1.ToString().Split(new Char[] { ',', '\n' });
            string[] every_course_status_student = t1.ToString().Split(new Char[] { ',', '\n' });
            string[] every_course_grade_student = b3.ToString().Split(new Char[] { ',', '\n' });
            string[] every_course_name_student = b6.ToString().Split(new Char[] { ',', '\n' });
            CGPA = Convert.ToDouble(b2.ToString());
            warning = Convert.ToInt32(b4.ToString());
            semester_student = Convert.ToInt32(b5.ToString());
            student_name = b8.ToString();
            student_rollnumber = b7.ToString();
            for (int k = 0; k < every_course_code_student.Length; k++)
            {
                studentcoursecodelist.Add(every_course_code_student[k]);
                statuslist.Add(every_course_status_student[k]);
                studenttakingcourses.Add(every_course_name_student[k]);
                studentcoursegrade.Add(Convert.ToDouble(every_course_grade_student[k]));

            }
            Connection1.connect();
            DataTable gt2 = Connection1.getCurrentCourses();
            Connection1.disconnect();
            object z1 = gt2.Rows[0]["COURSENAME"];
            object z2 = gt2.Rows[0]["CREDITHOURS"];

            string[] every_current_course_offered = z1.ToString().Split(new Char[] { ',', '\n' });
            string[] every_course_credit_offered = z2.ToString().Split(new Char[] { ',', '\n' });

            for (int i = 0; i < every_course_credit_offered.Length; i++)
            {
                CheckBoxList1.Items.Add(every_current_course_offered[i]);
                currentcourse.Add(every_current_course_offered[i]);
                currentcredits.Add(every_course_credit_offered[i]);

            }


            




        }
    }
   
  
    protected void Button1_Click(object sender, EventArgs e)
    {
        ListBox1.Items.Clear();
        bool test = true;
        bool test1 = true;
        int matchnumber = 0;
        int selectnumber;
        string not_selectedcourses = null;

        foreach (ListItem ear in CheckBoxList1.Items)
        {
            if (ear.Selected)
            {
                selectnumber = Convert.ToInt32(CheckBoxList1.Items.IndexOf(ear).ToString());
                // ListBox1.Items.Add(selectnumber.ToString());

                for (int z = 0; z < total_fastcourses.Count; z++)
                {
                    if (currentcourse[selectnumber] == total_fastcourses[z])
                    {
                        matchnumber = z;
                        z = total_fastcourses.Count;
                    }
                }
                // ListBox1.Items.Add(matchnumber.ToString() );
                if (list1[matchnumber].ToString() != "0")
                {
                    // ListBox1.Items.Add("not equal");

                    for (int i = 0; i < studentcoursecodelist.Count; i++)
                    {
                        if (list1[matchnumber].ToString() == studentcoursecodelist[i].ToString() && statuslist[i] == "YES")
                        {

                            setcoursestudentlist.Add(CheckBoxList1.Items[selectnumber].ToString());
                        }
                        if (list1[matchnumber].ToString() == studentcoursecodelist[i].ToString() && statuslist[i] == "NO")
                        {
                            not_selectedcourses += CheckBoxList1.Items[selectnumber].ToString();
                        }
                    }
                }
                if (list1[matchnumber].ToString() == "0")
                {
                    //ListBox1.Items.Add( total_courselist[matchnumber ].ToString());
                    //ListBox1.Items.Add(matchnumber.ToString());

                    setcoursestudentlist.Add(CheckBoxList1.Items[selectnumber].ToString());
                    // ListBox1.Items.Add(CheckBoxList1.Items[selectnumber].ToString());
                    // ListBox1.Items.Add("umer");



                    //  ListBox1.Items.Add(CheckBoxList1.Items[selectnumber].ToString());
                    //ListBox1.Items.Add("ok1");











                }

            }


        }


        if (setcoursestudentlist.Count > 0)
        {
            // ListBox1.Items.Clear();

            for (int i = 0; i < setcoursestudentlist.Count; i++)
            {

                for (int j = 0; j < total_fastcourses.Count; j++)
                {

                    if (setcoursestudentlist[i] == total_fastcourses[j])
                    {

                        if (list1[j] != "0")
                        {

                            for (int z = 0; z < total_courselist.Count; z++)
                            {
                                if (list1[j] == total_courselist[z])
                                {

                                    for (int z1 = 0; z1 < setcoursestudentlist.Count; z1++)
                                    {
                                        if (total_fastcourses[z] == setcoursestudentlist[z1])
                                        {


                                            test1 = false;

                                            not_selectedcourses += CheckBoxList1.Items[z].ToString();
                                        }
                                    }
                                    if (test1 == true)
                                    {
                                        ListBox1.Items.Add(setcoursestudentlist[i]);

                                    }

                                }
                            }
                        }
                        else
                        {

                            ListBox1.Items.Add(setcoursestudentlist[i]);



                        }

                    }

                }

            }

        }
        if (not_selectedcourses != null)
        {

            Response.Write("<script LANGUAGE='JavaScript' >alert('Can Not Take Some Courses Because not clear pre-requsit')</script>");

        }
        foreach (ListItem l in ListBox1.Items)
        {

           submit_application_courses.Add(l.ToString());

        }
        Session["one"] = submit_application_courses;
        Session["two"] = student_name;
        Session["three"] = student_rollnumber;
        Session["four"] = CGPA;
        setcoursestudentlist.Clear();

    }
}