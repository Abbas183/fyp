﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

/// <summary>
/// Summary description for CourseList
/// </summary>
public class CourseList
{
   public List<Course> list;
   List<string> mylist_course = new List<string>();
   List<string> mylist_credit = new List<string>();
   List<string> mylist_code = new List<string>();
   public static int length;
   string course = null;
   double grade;
   public static int set = 0;
   static double CGPA = 2;
  string [] course_pass_code=new string[]{"CS101","MT101","MT104","NS101","SS102"};
      double []   course_grade = new double[] {1.0,1.0,2.0,1.0,1.0 };
     string []    current_course = new string[] { "CS101", "MT101", "MT104", "NS101", "SS102", "CS103","EE109","MT115","SS122","SS138","CS201", "CS211", "EE213", "SS111", "MGX01" };
      string [] total_course=new string[]{"CS101", "MT101", "MT104", "NS101", "SS102", "CS103","EE109","MT115","SS122","SS138","CS201", "CS211", "EE213", "SS111", "MGX01"};
      string[] pre_req = new string[] { "0", "0", "0", "0", "0", "CS101", "0", "MT101", "SS102", "0", "CS103", "0", "EE109", "0", "0" };
    public static string[] course_combination = new string[50];

	public CourseList()
	{
        list = new List<Course>();
	}
    public void GenerateCombination()
    {
      
        string my_course;
        string my_course1;
       
        //Label1.Text = course_pass_code.Length.ToString();
        for (int i = 0; i < 5; i++)
        {
            course = course_pass_code[i];
            grade = course_grade[i];
            if (grade <= 1)
            {

                for (int j = 0; j < current_course.Length; j++)
                {
                    if (course == current_course[j])
                    {

                        course_combination[set] = current_course[j];


                       // Label1.Text += "       " + course_combination[set];
                       // list1.Add(course_combination[set]);
                        set++;

                    }
                }
            }
            else
            {
                //Label2.Text = "umer";
                for (int x = 0; x < pre_req.Length; x++)
                {

                    if (course == pre_req[x])
                    {

                        for (int y = 0; y < current_course.Length; y++)
                        {
                            if (current_course[y] == total_course[x])
                            {
                                course_combination[set] = current_course[y];

                              

                               
                                set++;
                                // Label1.Text = set.ToString();


                            }
                        }

                    }
                }
            }



        }


        for (int i = 0; i < current_course.Length; i++)
        {
            bool test = false;
            bool test1 = false;

            for (int j = 0; j < set; j++)
            {
                if (course_combination[j] == current_course[i])
                {
                    // Label2.Text = i.ToString();
                    //Label1.Text = course_combination[j];
                    test = true;
                }
            }
            if (test == false)
            {
                for (int k = 0; k < course_pass_code.Length; k++)
                {
                    if (course_pass_code[k] == current_course[i])
                    {
                        // course_combination[set] = current_course[i];
                        //Label1.Text += course_combination[set];
                        //set++;
                        //Label1.Text = set.ToString();
                        test1 = true;

                    }

                }
                if (test1 == false)
                {
                    course_combination[set] = current_course[i];
                    //Label1.Text += "       " + course_combination[set];
                    set++;
                }


            }


        }
        if (CGPA < 2.0)
        {
            
            //Label1.Text += "       " + course_combination[0] + "    " + course_combination[1] + "    " + course_combination[2] + "    ";

        }

    }
    public void SaveCombination()
    { 
    
    
    }
   
    public void AddCourses()
    {
        DataBaseHandler.connect();
        DataTable gt = DataBaseHandler.getcourses();
        DataBaseHandler.disconnect();
        if (gt.Rows.Count == 0)
        {

        }


        object a = gt.Rows[0]["Current_courses"];
        object b = gt.Rows[0]["Credit_hours"];
        object c = gt.Rows[0]["Course_code"];
        string course = a.ToString();
        string credithours = b.ToString();
        string code = c.ToString();
        string[] every_course = course.Split(new Char[] { ',', '\n' });
        string[] every_credit_hours = credithours.Split(new Char[] { ',', '\n' });
        string[] every_code = code.Split(new Char[] { ',', '\n' });
        length = every_code.Length;
        for (int i = 0; i < every_code.Length; i++)
        {
            mylist_code.Add(every_code[i]);
            mylist_course.Add(every_course[i]);
            mylist_credit.Add(every_credit_hours[i]);
        }
    }
    public void ShowCombination()
    {

      
    }
    public void selectCombination(String combination)
    {
        String course_combination = combination;
    }
    public List<Course> GetCourses()
    {
        Course obj = new Course();
        for (int i = 0; i < mylist_code.Count; i++)
        {
            obj.SetCourses(Convert.ToInt32(mylist_credit[i].ToString()),mylist_course[i].ToString(),mylist_code[i].ToString());
        }

        
        return list;
    }

}