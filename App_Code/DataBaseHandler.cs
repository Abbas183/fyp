﻿using System.Text;
using System.Data.OleDb;
using System.Data.Odbc;
using System.Linq;
using System.Web;
using System.Data;
using System;

/// <summary>
/// Summary description for DataBaseHandler
/// </summary>
public class DataBaseHandler
{
	
    public static OleDbConnection con;
    public static OleDbCommand cmd;
    public static OleDbDataAdapter adapter;


    public DataBaseHandler()
    {

    }
    public static void connect()
    {
        string connectionstring = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=G:/FYP_database/project.accdb";
        con = new OleDbConnection(connectionstring);
    }
    public static void disconnect()
    {
        con.Close();
    }
    public static DataTable getstudent(string conc)
    {
        con.Open();
        String query = "select * from students where RollNo='" + conc + "'";
        cmd = con.CreateCommand();
        cmd.CommandText = query;
        adapter = new OleDbDataAdapter(cmd);
        DataTable dt = new DataTable();
        adapter.Fill(dt);
        return dt;
    }
    public static DataTable getcourses()
    {
        con.Open();
        String query = "select * from current_offered_courses";
        cmd = con.CreateCommand();
        cmd.CommandText = query;
        adapter = new OleDbDataAdapter(cmd);
        DataTable dt = new DataTable();
        adapter.Fill(dt);
        return dt;
    }
}