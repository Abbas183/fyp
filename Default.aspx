﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta charset="utf-8">

<link rel="stylesheet" media="screen" href="styles.css" >

    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <form class="contact_form" action="#" method="post" name="contact_form">
   
 <ul>
        <li>
             <h2>Contact Us</h2>
            
 <span class="required_notification">* Denotes Required Field</span>
     
   </li>
        <li>
            <label for="name">Name:</label>
         

   <input type="text"  placeholder="John Doe" required />
     
   </li>
        <li>
            <label for="email">Email:</label>
    
        <input type="email" name="email" placeholder="john_doe@example.com" required />
  
          <span class="form_hint">Proper format "name@something.com"</span>
        </li>
      
  <li>
            <label for="website">Website:</label>
       
     <input type="url" name="website" placeholder="http://johndoe.com" required pattern="(http|https)://.+"/>
       
     <span class="form_hint">Proper format "http://someaddress.com"</span>
        </li>
        <li>
         
   <label for="message">Message:</label>
            <textarea name="message" cols="40" rows="6" required ></textarea>
   
     </li>
        <li>
        	<button class="submit" type="submit">Submit Form</button>
    
    </li>
    </ul>
</form>


    
    </div>
    </form>
</body>
</html>
